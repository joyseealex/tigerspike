using landmarkremark.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace landmarkremark.Controller
{
  [Route("api/[controller]")]
  public class MapsController : ControllerBase
  {
    private readonly LandmarkDAL _landmarkDAL;

    public MapsController(LandmarkDAL landmarkDAL)
    {
      _landmarkDAL = landmarkDAL;
    }

    /// <summary>
    /// Gets all saved notes for the location
    /// </summary>
    /// <param name="lattitude">Gets lattitude</param>
    /// <param name="longitude">Gets longitude</param>
    /// <returns>Returns all the notes for the given coordinates</returns>
    // GET: api/<controller>
    [HttpGet]
    public IEnumerable<LocationNotes> GetLocationNotes()
    {
      var localNotesList = new List<LocationNotes>();
      var dbNotesList = _landmarkDAL.GetNotesFromLocation();
      foreach (var eachNote in dbNotesList)
      {
        var localNotes = new LocationNotes
        {
          LocationNotesId = eachNote.LocationNotesId,
          Lattitude = eachNote.Lattitude,
          Longitude = eachNote.Longitude,
          CreatedDate = eachNote.CreatedDate,
          Note = eachNote.Note
        };

        localNotesList.Add(localNotes);
      }

      return localNotesList;
    }

    /// <summary>
    /// Creates a new note for the location
    /// </summary>
    /// <param name="locationNotes">Gets all the details of the notes to be saved in db</param>
    [HttpPost("[action]")]
    public void CreateLocationNotes(LocationNotes locationNotes)
    {
      Models.LocationNotes dbLocationNotes = new Models.LocationNotes()
      {
        Lattitude = locationNotes.Lattitude,
        Longitude = locationNotes.Longitude,
        Note = locationNotes.Note
      };

      _landmarkDAL.CreateNotes(dbLocationNotes);
    }

    public class LocationNotes
    {
      public int LocationNotesId { get; set; }
      public decimal Lattitude { get; set; }
      public decimal Longitude { get; set; }
      public string Note { get; set; }
      public int MapUserId { get; set; }
      public DateTime CreatedDate { get; set; }
    }

    //public class LocationNotes
    //{
    //  public int LocationId { get; set; }
    //  public decimal Lattitude { get; set; }
    //  public decimal Longitude { get; set; }
    //  public Notes[] NotesList { get; set; }
    //}

    //public class Notes
    //{
    //  public int NoteId { get; set; }
    //  public string Message { get; set; }
    //  public int LocationId { get; set; }
    //}
  }
}
