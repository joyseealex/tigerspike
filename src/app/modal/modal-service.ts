import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class ModalService {
  private modals: any[] = [];  
  myAppUrl: string = "";
  public noteList: LocationNotesData[];

  constructor(private _http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.myAppUrl = baseUrl;
    this.getNotes();
  }

  add(modal: any) {
    // add modal to array of active modals
    this.modals.push(modal);
  }

  remove(id: string) {
    // remove modal from array of active modals
    this.modals = this.modals.filter(x => x.id !== id);
  }

  open(id: string) {
    let modal: any = this.modals.filter(x => x.id === id)[0];
    modal.open();
  }

  close(id: string) {
    // close modal specified by id
    let modal: any = this.modals.filter(x => x.id === id)[0];
    modal.close();
  }

  getNotes() {
    return this._http.get<LocationNotesData[]>(this.myAppUrl + 'api/Maps/GetLocationNotes').subscribe(result => {
      this.noteList = result;
    }, error => console.error(error));
  } 
}

interface LocationNotesData {
  LocationNotesId: number;
  Lattitude: number;
  Longitude: number;
  Notes: string;
}
