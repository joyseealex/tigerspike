import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ModalComponent } from './modal/modal-component';
import { ModalService } from './modal/modal-service';
import { ShowMapComponent } from './show-map/show-map.component';
export const BASE_URL = new InjectionToken<string>('BASE_URL');

@NgModule({
  declarations: [
    AppComponent,
    ModalComponent,
    ShowMapComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'show-map', component: ShowMapComponent }
    ])
  ],
  providers: [ModalService, { provide: BASE_URL, useValue: "https://localhost:44338/" }],
  bootstrap: [AppComponent]
})

export class AppModule { }
