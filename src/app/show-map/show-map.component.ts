import { Component, ViewChild, OnInit, Input, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { } from 'googlemaps';
import { ModalService } from '../modal/modal-service';

@Component({
  selector: 'app-show-map',
  templateUrl: './show-map.component.html'
})

export class ShowMapComponent implements OnInit {
  @ViewChild('map', { static: true }) mapElement: any;
  map: google.maps.Map;
  public currentPosition: {lat: string, lng: string};
  @Input() lattitude: number;
  @Input() longitude: number;
  myAppUrl: string = "";
  http: any;
  locationNoteForm: FormGroup;
  public noteList: LocationNotesData[];

  constructor(private modalService: ModalService, private _http: HttpClient, @Inject('BASE_URL') baseUrl: string, private _fb: FormBuilder) {
    this.myAppUrl = baseUrl;
    this.locationNoteForm = this._fb.group({
      note: ['', [Validators.required]]
    })
  }

  ngOnInit(): void {
    const mapProperties = {
      center: new google.maps.LatLng(-33.836823, 151.207597),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
    var infoWindow, map = this.map;
    infoWindow = new google.maps.InfoWindow;

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };     
       
        infoWindow.setPosition(pos);
        var content ='<p>You are here.</p>';
        infoWindow.setContent(content);
        infoWindow.open(map);
        map.setCenter(pos);
      }, function () {
        this.handleLocationError(true, infoWindow, map.getCenter(), map);
      });
    } else {
      // Browser doesn't support Geolocation
      this.handleLocationError(false, infoWindow, map.getCenter(), map);
    }
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  handleLocationError(browserHasGeolocation, infoWindow, pos, map): void {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
      'Error: The Geolocation service failed.' :
      'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
  }

  getNotes() {
    return this._http.get<LocationNotesData[]>(this.myAppUrl + 'api/Maps/GetLocationNotes').subscribe(result => {
      this.noteList = result;
    }, error => console.error(error));
  }

  saveNote(): void {
    this._http.post(this.myAppUrl + 'api/Maps/CreateLocationNotes', this.locationNoteForm.value).subscribe(result => {

    }, error => console.error(error));
  }

  get note() { return this.locationNoteForm.get('notes'); }
}

interface LocationNotesData {
  locationNotesId: number;
  lattitude: number;
  longitude: number;
  note: string;
}
