using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace landmarkremark.Models
{
  public class SeedData
  {
    public static void Initialize(IServiceProvider serviceProvider)
    {
      using (var context = new LandmarkContext(
          serviceProvider.GetRequiredService<
              DbContextOptions<LandmarkContext>>()))
      {
        // Look for any movies.
        if (context.MapUsers.Any())
        {
          return;   // DB has been seeded
        }

        context.MapUsers.AddRange(
            new MapUsers
            {
              MapUserId = 1,
              FirstName = "Joysee",
              LastName = "Alex"
            },

            new MapUsers
            {
              MapUserId = 2,
              FirstName = "Mithun",
              LastName = "Mathews"
            },

            new MapUsers
            {
              MapUserId = 3,
              FirstName = "Noah",
              LastName = "Mathews"
            }
        );

        context.SaveChanges();

      }
    }
  }
}
