using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace landmarkremark.Models
{
  public class LandmarkDAL
  {
    /// <summary>
    /// Db context
    /// </summary>
    private readonly LandmarkContext _context;

    /// <summary>
    /// Landmark Constructor
    /// </summary>
    /// <param name="context"></param>
    public LandmarkDAL(LandmarkContext context)
    {
      _context = context;
    }

    /// <summary>
    /// Function to create notes for the location received.
    /// </summary>
    /// <param name="notes">Gets notes to be saved.</param>
    public void CreateNotes(LocationNotes notes)
    {
      LocationNotes dbLocationNotes = new LocationNotes
      {
        Lattitude = notes.Lattitude,
        Longitude = notes.Longitude,
        Note = notes.Note,
        CreatedDate = DateTime.Now,
        MapUserId = 1
      };

      _context.LocationNotes.Add(dbLocationNotes);
      _context.SaveChanges();
    }

    /// <summary>
    /// Function to get all notes based on the given coordinates
    /// </summary>
    /// <param name="lattitude">Gets the Lattitude</param>
    /// <param name="longitude">Gets the Longitude</param>
    /// <returns>Returns the notes based on the Location coordinates</returns>
    public IEnumerable<LocationNotes> GetNotesFromLocation()
    {
      //Where(notes => notes.Lattitude == lattitude && notes.Longitude == longitude)
      return _context.LocationNotes;
    }
  }
}
