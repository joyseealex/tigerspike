using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace landmarkremark.Models
{
  public class MapUsers
  {
    [Key]
    public int MapUserId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }

    public virtual ICollection<LocationNotes> LocationNotes { get; set; }
  }

  public class LocationNotes
  {
    [Key]
    public int LocationNotesId { get; set; }
    public decimal Lattitude { get; set; }
    public decimal Longitude { get; set; }
    public string Note { get; set; }
    public int MapUserId { get; set; }
    public DateTime CreatedDate { get; set; }
    public virtual MapUsers MapUsers { get; set; }
  }
}
