using Microsoft.EntityFrameworkCore;

namespace landmarkremark.Models
{
  public class LandmarkContext : DbContext
  {
    public LandmarkContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<MapUsers> MapUsers { get; set; }

    public DbSet<LocationNotes> LocationNotes { get; set; }
  }
}
